//
//  ViewController.swift
//  Stopwatch
//
//  Created by Xiao Xiong Mao on 9/19/14.
//  Copyright (c) 2014 Aniyatech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var splitSec = 0
    var splitMin = 0
    var splitHour = 0
    
    var ongoingSec = 0
    var ongoingMin = 0
    var ongoingHour = 0
    
    var timer = NSTimer()
    var timerRunning = false
    var paused = true
    var pauseTotal = true
    
    @IBOutlet weak var ongoingLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startButton: UIBarButtonItem!
    @IBOutlet weak var pauseButton: UIBarButtonItem!
    
    @IBAction func stopButtonPressed(sender: AnyObject) {
        timer.invalidate()
        timerRunning = false
    }

    @IBAction func pauseButtonPressed(sender: AnyObject) {
        paused = true
    }
   
    @IBAction func startButtonPressed(sender: AnyObject) {
        if (!timerRunning) {
            timerRunning = true
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
        }
        
        paused = false
    }
    
    @IBAction func resetButtonPressed(sender: AnyObject) {
        splitSec = 0
        splitMin = 0
        splitHour = 0
        
        ongoingSec = 0
        ongoingMin = 0
        ongoingHour = 0
        
        displayTime()
    }
    
    func displayTime() {
        let splitTime = String(format:"%02d:%02d:%02d",splitHour,splitMin,splitSec)
        let ongoingTime = String(format:"%02d:%02d:%02d",ongoingHour,ongoingMin,ongoingSec)
        
        timerLabel.text = splitTime
        ongoingLabel.text = ongoingTime
    }
    
    func timerAction() {
        if (!paused) {
            splitSec++
            
            if (splitSec == 60) {
                splitSec = 0
                splitMin++
                
                if (splitMin == 60) {
                    splitMin = 0
                    splitHour++
                }
            }
        }
        
        ongoingSec++
        
        if (ongoingSec == 60) {
            ongoingSec = 0
            ongoingMin++
            
            if (ongoingMin == 60) {
                ongoingMin = 0
                ongoingHour++
            }
        }
        
        displayTime()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

